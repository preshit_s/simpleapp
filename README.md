# SimpleApp - Preshit Satalgaonkar

## Description

The project is a simple form submission which takes in the user name and the email id to register the user on the website.

## Installation

1. Install and configure CI/CD tool: Install and configure your chosen CI/CD tool (e.g., Jenkins, GitLab CI/CD) on a server or cloud instance.

2. Install required dependencies: Install any dependencies required by your CI/CD pipeline, such as node.js, npm, docker etc.

## Configuration

The Jenkinsfile is included in the project repository. It has four stages namely `Checkout`, `Build`, `Test` and `Deploy`.

Checkout - Checkouts the source code from the gitlab repository using the gitlab credentials and the gitlab project url.
(You should enter your own credentials and url to run this pipeline at your end.)

Build - Builds the webapp using npm.

Test - Installs the linting package and runs the tests.

Deploy - Deploys the webapp using the Jenkinsfile.

## Webhook

Webhook is used to automatically trigger the Jenkinsfile on each of the commits to the gitlab project repository. This is done by linking the Jenkinsfile to the repository using the .gitlab-ci.yml file. As soon as a developer pushes a commit to the gitlab repository, this would trigger the gitlab pipeline which in turn would trigger the Jenkinsfile pipeline.

The payload URL required to create a Webhook wont accept a localhost url.As Jenkins runs on the localhost, we would need some other mechanism to create a webhook. This issue is solved using ngrok. ngrok maps the localhost url to an internet based url which makes it possible to create a webhook.

![Webhook](images/Screenshot__38_.png)

## Running the CI/CD pipeline

1. Commit and Push Changes: Make changes to your project code and commit them to the repository. Push the changes to your Version Control System.
2. Automatic Triggering: Your CI/CD pipeline should be automatically triggered by the webhook or trigger configured in the Version Control System. Basically, It should execute job in the jenkins project/intent.
3. Monitor Pipeline Execution: Monitor the execution of your CI/CD pipeline in the CI/CD tool's dashboard or interface. Check the logs and output of each stage and job to ensure everything is running smoothly.

![Pipeline Overview](images/Screenshot__37_.png)

![Pipeline Stages](images/Screenshot__36_.png)

![Console Output](images/Screenshot__33_.png)

![Console Output](images/Screenshot__34_.png)

## Project

![Project](images/Screenshot__35_.png)
