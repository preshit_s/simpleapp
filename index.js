const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});


app.post('/submit', (req, res) => {
    const formData = req.body;
    console.log('Form data received:', formData);
    res.send(formData);
});

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});